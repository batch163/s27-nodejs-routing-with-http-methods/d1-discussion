const HTTP = require('http');
const PORT = 3002;

// Mock database
let directory = [
    {
        "name": "Brandon",
        "email": "brandon@mail.com"
    },
    {
        "name": "Jobert",
        "email": "jobert@mail.com"
    }
];

HTTP.createServer((req, res) => {
    // console.log("test")

    if(req.url == "/users" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("This is from GET request")

    } else if(req.url == "/products" && req.method == "POST"){

        let reqBody = "";

        req.on("data", (data) => {
            console.log(data)       //<Buffer 7b 0d 0a 20 20 20 20 22 75 73 65 72 22 3a 20 22 6a 6f 79 22 2c 0d 0a 20 20 20 20 22 70 72 6f 64 75 63 74 22 3a 20 22 62 61 72 62 69 65 22 0d 0a 7d>
            console.log(typeof data)    //object

            reqBody += data
           
        })

         req.on("end", () => {
            console.log(reqBody)            //{"user": "joy", "product": "barbie"}
            console.log(typeof reqBody)             //string
          
            console.log(JSON.parse(reqBody))        //{ user: 'joy', product: 'barbie' }
            reqBody = JSON.parse(reqBody)           
        //     //we parse the json payload to be able to use javascript logic such as array and object methods

            console.log(reqBody)                    //{ user: 'joy', product: 'barbie' }

            const sentence = `Hi, I'm ${reqBody.user} and I love ${reqBody.product} toys!`


            res.writeHead(200, {"Content-Type": "text/plain"})
            res.write(JSON.stringify(sentence)) //we use stringify method because server cannot handle JS objects to transfer data back to the client, it has to be in a form of string
            res.end()
        })
    } else if(req.url == "/directory" && req.method == "POST"){

        let reqBody = "";
        // Mini Activity:

        // Create a new route "/directory" and http method to POST

        req.on("data", (data) => {
            reqBody += data
        })

        req.on("end", () => {
            reqBody = JSON.parse(reqBody)
            // console.log(typeof reqBody)

            // add the new object to be received from the request body into the mock database
            directory.push(reqBody)
            // return the whole mock database (including the newly added object) as a response to the client.
            res.writeHead(200, {"Content-Type": "text/plain"})
            res.write(JSON.stringify(directory))
            res.end()
        }) 
    }

}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));